import java.net.MalformedURLException;
import java.net.URL;
import java.security.cert.Certificate;
import java.io.*;
//import javax.net.ssl.HttpURLConnection;
import javax.net.ssl.SSLPeerUnverifiedException;
import java.util.ArrayList;
import java.net.HttpURLConnection;
///v1/country/type[/GCM]/var/start/end/ISO3
public abstract class RequeteRegion{
    private String url_base="http://climatedataapi.worldbank.org/climateweb/rest/v1/country/";
   /*public static void main(String[] args)
   {
        new RequeteRegion().fetch("http://climatedataapi.worldbank.org/climateweb/rest/v1/basin/type/var/start/end/123"
           //"http://climatedataapi.worldbank.org/climateweb/rest/v1/country/mavg/bccr_bcm2_0/a2/tas/2020/2039/fra");
       
   }*/
    
   public String buildQuery(String type,String var,int dateDebut,int dateFin,String isoCode){
    return url_base+type+'/'+"bccr_bcm2_0/a2/"+var+'/'+dateDebut+'/'+dateFin+'/'+isoCode;
   }
   



   public ArrayList fetch(String http_url){
      ArrayList res=new ArrayList();
      String buffer="";
      char temp;
      URL url;
      try {

	     url = new URL(http_url);
        HttpURLConnection con = (HttpURLConnection)url.openConnection();
        
	     //parser le contenu String pour avoir un arraylist de float des previsions mensuels
         String  content= get_content(con);
         for(int i=1;i<content.length();i++){
            temp=content.charAt(i);
            if(temp==']' || temp==',' ){
               res.add(Float.parseFloat(buffer));
               buffer="";
               continue;
            }else{
               buffer+=temp;
            }
         }
         return res;
         
      } catch (MalformedURLException e) {
	     e.printStackTrace();
      } catch (IOException e) {
	     e.printStackTrace();
      }
      return res;
   }
	
	
   public String get_content(HttpURLConnection con){
		 String content="";
		 
	if(con!=null){
		
	try {
		
	   //System.out.println("****** Content of the URL ********");			
	   BufferedReader br = 
		new BufferedReader(
			new InputStreamReader(con.getInputStream()));
				
	   String input;
				
	   while ((input = br.readLine()) != null){
          System.out.println();
          for (int i=67;i<input.length()-32;i++){
             content+=input.charAt(i);
          }
			 return (content);

       }
       br.close();
       
	   
				
	} catch (IOException e) {
	   e.printStackTrace();
	}
			
       }
       return content;
   }
   /*public void test(){
      for(int i=1;i<469;i++){
         String http_url="http://climatedataapi.worldbank.org/climateweb/rest/v1/basin/type/var/start/end/"+i;
         URL url = new URL(http_url);
	      HttpURLConnection con = (HttpURLConnection)url.openConnection();
         System.out.println(get_content(con));
      }
      
   }*/
	
}