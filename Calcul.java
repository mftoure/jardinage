import java.util.*;


public class Calcul{
    
    public static void main(String[] args){
        
        RequeteUtilisateur ru=new RequeteUtilisateur();
        Region region=new Region(ru.dateDebut,ru.dateFin,ru.codeIso);
        
        float tempmin=region.Find(region.temperature, true)*1.8f+32.00f;
        float rainmin=region.Find(region.precipitation,true);
        float rainmax=region.Find(region.precipitation,false);
        
        ru.Params.add("precipitation_maximum<"+rainmax);
        ru.Params.add("precipitation_minimum>"+rainmin);
        ru.Params.add("temperature_minimum_deg_f>"+tempmin);
        ru.Params.add("page_size=100");
        //System.out.println(ru.Params);
        Plante p=new Plante("plants",ru.Params);

        if(p.res.length()>3){
            System.out.println("Voici les noms scientifiques des plantes satifaisant votre requete");
            p.ParserRes();
        }
        else{
            System.out.println("Desole, aucune plante satisfait vos conditions");
        }
        
        
        
       

        
        
        
    }

}