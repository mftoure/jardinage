import java.io.IOException;
import java.util.*;
import java.util.ArrayList;

public class RequeteUtilisateur{
    //attributs region
    //public String nomPays;
    public String codeIso;
    public int dateDebut;
    public int dateFin;
    //attributs plante
    public ArrayList Params;

    
    public RequeteUtilisateur(String codeIso,int dateDebut,int dateFin){
        this.codeIso=codeIso;
        this.dateDebut=dateDebut;
        this.dateFin=dateFin;
        this.Params=new ArrayList<>();
        //this.region=new Region(dateDebut,dateFin,codeIso);
    }
    public RequeteUtilisateur(String codeIso,int dateDebut,int dateFin,ArrayList params){
        this(codeIso,dateDebut,dateFin);
        this.Params=params;
    }
    public RequeteUtilisateur(){
        //this.codeIso=EntreeUtilisateur().codeIso;
        //this.dateDebut=EntreeUtilisateur().dateDebut;
        //this.dateFin=EntreeUtilisateur().dateFin;
        EntreeUtilisateur();
        
    }

    public  void EntreeUtilisateur(){
        //CONTROLES A RAJOUTER: ENTREE INT POUR DATE DEBUT ET FIN
        //Requete region et date
        print("Bonjour!");
        Scanner scan = new Scanner(System.in);
        print("etape 1: saisir un code iso de trois lettre, par exemple fra pour France");
        String Iso = scan.next();
        //Controle code iso valide
        while(Iso.length()>3){
            print("Erreur, veuilleuz resaissir un code pays a trois lettres!");
            Iso = scan.next();
        }
        codeIso=Iso;
        print("Etape 2: saisir une date de debut parmis les annees suivantes:");
        print("2020, 2040, 2060,2080");
        ArrayList c1=new ArrayList(); c1.add(2020);c1.add(2040);c1.add(2060);c1.add(2080);
        //int dateDebut=0;
        boolean control=true;
        //controle date valide
        while(control){
            print("saisir un date debut valide parmis les annees suivantes, la fenetre de previsions sera entre la date choisi et 19 ans apres:");
            print("2020, 2040, 2060,2080");
            try {
                int Debut= scan.nextInt();
                if(c1.contains(Debut)){
                    dateDebut=Debut;
                    control=false;
                    if(Debut==(2020)){dateFin=2039;}
                    if(Debut==(2040)){dateFin=2059;}
                    if(Debut==(2060)){dateFin=2079;}
                    if(Debut==(2080)){dateFin=2099;}
                }
            }
            catch(java.util.InputMismatchException e){
                print("erreur ,saissisez un entier valables pour la date de debut");
                scan.next();
            
            }
            
        }
        

        //Requete Plante
        print("Vous pouvez egalement faire un recherche plus poussee en rentrant le ph de votre sol , ainsi que le type de plante souhaitee");
        print("tapez Y pour ajouter ces parametres et N sinon");
        String reponse= scan.next();
        if (reponse.equals("n") || reponse.equals("N")){
            Params=new ArrayList<>();
            
        }else{
            ArrayList <String> params=new ArrayList<String>();

            ArrayList <String> requetes=new ArrayList<String>(Arrays.asList("1-PH minimum du sol: de type float","2-PH maximum : de type float","3-Alimentation animale: de type boolean","4-Consomable par des humain : de type boolean"));

            ArrayList  indicesPossibles=new ArrayList<>(Arrays.asList(1,2,3,4));
            print("Voici les parametres que vous pouvez choisir");
            System.out.println(requetes);

            //CONTROLES ENTREES UTILISATEUR
            if (reponse.equals("y") || reponse.equals("Y")){
                int nbRequetes=0;
                while(requetes.size()>0){
                    print("Saissisez un entier entre 1 et 4 correspondant au numero devant les parametres afin de definir celui de votre choix, ou tapez N pour quitter");
                    System.out.println(requetes);
                    
                    String exit=scan.next();
                    if( exit.equals("N")||exit.equals("n")){
                        print("Recherche de plantes satisfaisant votre requete, merci d'attendre un instant ");
                        break;
                    }else{
                    try {
                        int indice= (scan.nextInt());
                        if(indicesPossibles.contains(indice)){
                            //System.out.println(indicesPossibles.indexOf(indice));
                            requetes.remove(indicesPossibles.indexOf(indice));
                            indicesPossibles.remove(indicesPossibles.indexOf(indice));
                            print("Parametre choisi:");
                            //System.out.println(requetes.get(indice-1));
                            
                            
                        switch(indice){
                            case(1):
                                control=true;
                                while(control){
                                    try{
                                        print("PH Min");
                                        print("entrez un float");
                                        float ph_minimum=scan.nextFloat();
                                        params.add("ph_minimum="+ph_minimum);
                                        control=false;
                                        
                                    }
                                    catch(java.util.InputMismatchException e){
                                        print("erreur ,saissisez un float pour le ph minimum");
                                        scan.next();
                                    }
                                }
                                continue;
                        
                            case(2):
                                control=true;
                                while(control){
                                    try{
                                        print("PH Max");
                                        print("entrez un float");
                                        float ph_maximum=scan.nextFloat();
                                        params.add("ph_maximum="+ph_maximum);
                                        control=false;
                                        continue;
                                    }
                                    catch(java.util.InputMismatchException e){
                                        print("erreur ,saissisez un float pour le ph maximum");
                                        scan.next();
                                    }
                                }
                                continue;
                            case(3):
                                control=true;
                                while(control){
                                    try{
                                        print("Animal");
                                        print("entrez un boolean");
                                        boolean anim=scan.nextBoolean();
                                        if(anim){
                                            params.add("palatable_graze_animal="+anim);}
                                        control=false;
                                        
                                    }
                                    catch(java.util.InputMismatchException e){
                                        print("erreur ,saissisez un boolean pour l'alimentation animale");
                                        scan.next();
                                    }
                                }
                                continue;
                            case(4):
                                control=true;
                                while(control){
                                    try{
                                        print("humain");
                                        print("entrez un boolean");
                                        boolean anim=scan.nextBoolean();
                                        if(anim){   
                                            params.add("palatable_graze_human="+anim);
                                        }
                                        control=false;
                                    }
                                    catch(java.util.InputMismatchException e){
                                        print("erreur ,saissisez un float pour l'alimentation humain");
                                        scan.next();
                                    }
                                }



                        }
                            
                        }else{
                            print("Erreur, saisissez un indice valable");
                        }
                    }
                    catch (java.util.InputMismatchException e) {
                        scan.next();
                        
                        
                        print("Erreur, saisissez un entier valable");
                    }

                }
            }
            }
            Params=params;
        }
        
        
    }

    public void print(String s){
        System.out.println(s);
    }

}

