import java.util.ArrayList;

public class Region extends RequeteRegion{
    public ArrayList <Float> precipitation;
    public ArrayList <Float> temperature;

    public Region(int dateDebut,int dateFin,String isoCode){
        String precip_url=buildQuery("mavg", "pr", dateDebut, dateFin, isoCode);
        String temp_url=buildQuery("mavg", "tas", dateDebut, dateFin, isoCode);
        precipitation=super.fetch(precip_url);
        temperature=super.fetch(temp_url);
        
    }
    public float Find(ArrayList <Float> tab,boolean min_or_max){
        //si min_or_max=true on recherche le minimum,sinon retourne le maximum
        float var;
        if(min_or_max){    
            var=tab.get(0);
            for(int i=1;i<tab.size()-1;i++){
                if(tab.get(i)<var) var=tab.get(i);
            }
        }else{
            var=tab.get(0);
            for(int i=1;i<tab.size()-1;i++){
                if(tab.get(i)>var) var=tab.get(i);
            }
        }
        return var;
    }
    

}