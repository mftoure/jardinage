import java.util.*;

public  class Plante extends RequetePlante{
    public String res;
    private String nom;
    private String url;
    public Plante(String typeRequete){
        url=super.buildQuery(typeRequete);
        super.fetch(url);
    }
    public Plante(String typeRequete,ArrayList <String> params){
        url=super.buildQuery(typeRequete,params);
        this.res=super.fetch(url);
    
    }
    //ajouter fonction pour parser reponse json?
    public void ParserRes(){
        String buffer,unePlante="";
        for(int i=0;i<res.length()-4;i++){
            buffer=res.substring(i, i+3);
            if(buffer.equals("},{")){
                ParserPlante(unePlante);
                unePlante="";
            }else{
                unePlante+=res.charAt(i);
            }
        }
    }
    public void ParserPlante(String plante){
        String buffer,name="";
        int j=0;
        for(int i=0;i<plante.length()-17;i++){
            buffer=plante.substring(i, i+15);
            if( buffer.equals("scientific_name")){
                while(plante.charAt(i+j)!=','){
                    name+=plante.charAt(i+j);
                    j++;
                }
            }
        }
        System.out.println(name);
    }

     
}