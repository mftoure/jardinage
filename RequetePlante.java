import java.net.MalformedURLException;
import java.net.URL;
import java.security.cert.Certificate;
import java.io.*;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLPeerUnverifiedException;
import java.util.ArrayList;


public abstract class RequetePlante{
    private final String url_basique="https://trefle.io/api/";
    private final String token="?token=bEhqVk9QQzA3NS9VUEhOMG1jbGpGUT09";
    
   public String buildQuery(String typeRequete){
    return url_basique+typeRequete+'/'+token;
   }
   public String buildQuery(String typeRequete,ArrayList<String> params){
      String res= url_basique+typeRequete+'/'+token;
      for(int i=0;i<params.size();i++){
         res+='&'+params.get(i);
      }
      return res;
   }

   public String fetch(String https_url){
      //reste a convertir la reponse en format Json String en objet Json
      URL url;
      try {

	     url = new URL(https_url);
	     HttpsURLConnection con = (HttpsURLConnection)url.openConnection();
			
	    ;
			
	     //dump all the content
         String content=get_content(con);
         return (content);
			
      } catch (MalformedURLException e) {
	     e.printStackTrace();
      } catch (IOException e) {
	     e.printStackTrace();
      }
      return "Pas de plantes satisfaisant la requete";
   }
	
	
   public String get_content(HttpsURLConnection con){
       String content="";
	if(con!=null){
			
	try {
		
	   //System.out.println("****** Content of the URL ********");			
	   BufferedReader br = 
		new BufferedReader(
			new InputStreamReader(con.getInputStream()));
				
	   String input;
				
	   while ((input = br.readLine()) != null){
          //System.out.println(input);
          content+=input;
       }
       br.close();
       
	   
				
	} catch (IOException e) {
	   e.printStackTrace();
	}
			
       }
       return content;
   }
	
}